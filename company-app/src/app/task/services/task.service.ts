import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { Task } from '../models/task'
import { Observable } from 'rxjs';
import { TaskCreate } from '../models/task-create';
import { HttpResponse } from '@angular/common/http';
import { TaskUpdate } from '../models/task-update';

@Injectable({
	providedIn: 'root'
})
export class TaskService {
    public routePrefix = 'api/tasks';

    constructor(private httpService: HttpService) { }
    
    public getTasks(): Observable<Task[]> {
        return this.httpService.get<Task[]>(this.routePrefix);
    }

    public getTaskById(id: number): Observable<Task> {
        return this.httpService.get<Task>(`${this.routePrefix}/${id}`);
    }

    public postTask(taskCreate: TaskCreate): Observable<HttpResponse<TaskCreate>> {
        return this.httpService.postWithFullResponse<TaskCreate>(this.routePrefix, taskCreate);
    }

    public deleteTask(task: Task): Observable<Object> {
        return this.httpService.delete(`${this.routePrefix}/${task.id}`);
    }

    public updateTask(taskUpdate: TaskUpdate): Observable<Object> {
        return this.httpService.put<TaskUpdate>(this.routePrefix, taskUpdate);
    }
}
