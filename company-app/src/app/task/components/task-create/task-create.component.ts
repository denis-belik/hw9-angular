import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { TaskService } from '../../services/task.service';
import { Task } from '../../models/task';
import { TaskCreate } from '../../models/task-create';
import { Validators } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';
import { TaskState } from '../../models/task-state';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-task-create',
	templateUrl: './task-create.component.html',
	styleUrls: ['./task-create.component.css']
})
export class TaskCreateComponent implements OnInit, OnDestroy {
    @Output() taskCreated = new EventEmitter<TaskCreate>();

    public isLoading: boolean = false;

    public maxNameLength = 128;
    public maxDescLength = 1000;
    public minId = 1;

    private unsubscribe$ = new Subject<void>();

    form = new FormGroup({
        name: new FormControl('', [Validators.required, Validators.maxLength(this.maxNameLength)]),
        description: new FormControl('', [Validators.required, Validators.maxLength(this.maxDescLength)]),
        createdAt: new FormControl(new Date(), [Validators.required]),
        finishedAt: new FormControl('', [Validators.required]),
        projectId: new FormControl('', [Validators.required, Validators.min(this.minId)]),
        performerId: new FormControl('', [Validators.required, Validators.min(this.minId)])
    });

	constructor(
        private taskService: TaskService,
        private toastr: ToastrService) 
    { }

	ngOnInit(): void {
    }
    
    public onSubmit(): void {
        this.isLoading = true;

        const taskCreate: TaskCreate = <TaskCreate>this.form.value;

        let createdTask: Task;
        this.taskService.postTask(taskCreate)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response: HttpResponse<TaskCreate>) => {
                const body: TaskCreate = response.body;
                const taskId: number = this.getIdFromLocation(response.headers.get('location'));

                createdTask = {
                    id: taskId,
                    name: body.name,
                    description: body.description,
                    createdAt: body.createdAt,
                    finishedAt: body.finishedAt,
                    state: TaskState.Created,
                    projectId: body.projectId,
                    performerId: body.performerId
                };

                this.isLoading = false;
                this.toastr.success('Task created!');
                this.taskCreated.emit(createdTask);
            },
            (error) => {
                this.isLoading = false;
                console.log(error);   
                this.toastr.error('Something went wrong', 'Oops');
            }
        )
    }

    private getIdFromLocation(str: string): number {
        const startingIndex: number = str.lastIndexOf('/') + 1;
        return Number(str.slice(startingIndex, str.length));
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}

