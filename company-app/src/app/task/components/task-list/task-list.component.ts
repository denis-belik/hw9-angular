import { Component, OnInit, OnDestroy } from '@angular/core';
import { Task } from '../../models/task'
import { TaskService } from '../../services/task.service'
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-task-list',
	templateUrl: './task-list.component.html',
	styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit, OnDestroy {

	public tasks: Task[];

    public isTaskCreation: boolean = false;
    public isLoading: boolean = false;

    private unsubscribe$ = new Subject<void>();
    
    constructor(
        private taskService: TaskService,
        private toastr: ToastrService) { }

	ngOnInit(): void {
        this.isLoading = true;

        this.taskService.getTasks().subscribe(
            (body: Task[]) => {
                this.tasks = body;
                this.isLoading = false;
            },
            (error) => {
                this.isLoading = false;
                console.log(error);   
                this.toastr.error('Something went wrong', 'Oops');
            }     
        )
    }
    
    public addTaskToList(task: Task): void {
        this.tasks.push(task);
        this.isTaskCreation = false;
    }

    public deleteTask(task: Task): void {
        this.isLoading = true;
        this.taskService.deleteTask(task)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(() => {
                this.deleteFromList(task);
                this.toastr.success('Task deleted!');
                this.isLoading = false;
            },
            (error) => {
                this.isLoading = false;
                console.log(error);   
                this.toastr.error('Something went wrong', 'Oops');
            }
        );
    }

    public deleteFromList(task: Task): void {
        const taskIndex: number = this.tasks.findIndex((value) => value.id == task.id);
        this.tasks.splice(taskIndex, 1);
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
