import { Component, OnInit, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Task } from '../../models/task';
import { TaskUpdate } from '../../models/task-update';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TaskService } from '../../services/task.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-task-update',
	templateUrl: './task-update.component.html',
	styleUrls: ['./task-update.component.css']
})
export class TaskUpdateComponent implements OnInit, OnDestroy {

    @Input() task: Task;
    @Output() taskUpdated = new EventEmitter<Task>();
    @Output() updateCanceled = new EventEmitter<void>();

    public isLoading: boolean = false;

    public maxNameLength = 128;
    public maxDescLength = 1000;
    public minId = 1;

    private unsubscribe$ = new Subject<void>();

    states: string[] = [
        'Created',
        'Started',
        'Finished',
        'Canceled',
    ]

    public form: FormGroup;
	constructor(
        private taskService: TaskService,
        private toastr: ToastrService) 
        { }

	ngOnInit(): void {
        this.form = new FormGroup({
            name: new FormControl(this.task.name, [Validators.required, Validators.maxLength(this.maxNameLength)]),
            description: new FormControl(this.task.description, [Validators.required, Validators.maxLength(this.maxDescLength)]),
            finishedAt: new FormControl(this.task.finishedAt, [Validators.required]),
            state: new FormControl(this.task.state, [Validators.min(0), Validators.max(3)]),
            performerId: new FormControl(this.task.performerId, [Validators.min(this.minId)])
        });
	}

    public cancelUpdate(): void {
        this.updateCanceled.emit();
    }

    public onSubmit(): void {
        this.isLoading = true;

        const controls = this.form.controls;

        const taskUpdate: TaskUpdate = {
            id: this.task.id,
            name: controls.name.value ? controls.name.value : this.task.name,
            description: controls.description.value ? controls.description.value : this.task.description,
            finishedAt: controls.finishedAt.value ? controls.finishedAt.value : this.task.finishedAt,
            state: this.states.indexOf(controls.state.value) != -1 ? this.states.indexOf(controls.state.value) : this.task.state,
            performerId: controls.performerId.value ? controls.performerId.value : this.task.performerId
        }

        this.taskService.updateTask(taskUpdate)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(() => {
                this.task = {
                    id: this.task.id,
                    projectId: this.task.projectId,
                    createdAt: this.task.createdAt,
                    name: taskUpdate.name,
                    description: taskUpdate.description,
                    finishedAt: taskUpdate.finishedAt,
                    state: taskUpdate.state,
                    performerId: taskUpdate.performerId
                }
                
                this.isLoading = false;
                this.toastr.success('Task updated!');
                this.taskUpdated.emit(this.task);
            },
            (error) => {
                this.isLoading = false;
                console.log(error);   
                this.toastr.error('Something went wrong', 'Oops');
            }
        );
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}

