import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../../models/task';

@Component({
	selector: 'app-task',
	templateUrl: './task.component.html',
	styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

    @Input() task: Task;

    @Output() taskDelete = new EventEmitter<Task>();

    public isTaskUpdating = false;

	constructor() { }

	ngOnInit(): void {
	}

    public onDelete(): void {
        this.taskDelete.emit(this.task);
    }

    public updateList(updatedTask: Task): void {
        this.task = updatedTask;
        this.isTaskUpdating= false;
    }
}
