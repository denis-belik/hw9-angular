import { NgModule } from '@angular/core';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskComponent } from './components/task/task.component';
import { TaskCreateComponent } from './components/task-create/task-create.component';
import { TaskUpdateComponent } from './components/task-update/task-update.component';
import { SharedModule } from '../shared/shared.module';
import { TaskStateColorDirective } from './directives/task-state-color.directive'


@NgModule({
	declarations: [TaskListComponent, TaskComponent, TaskCreateComponent, TaskUpdateComponent, TaskStateColorDirective],
	imports: [
        SharedModule
	]
})
export class TaskModule { }
