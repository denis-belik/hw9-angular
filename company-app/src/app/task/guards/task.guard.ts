import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { TaskListComponent } from '../components/task-list/task-list.component'

@Injectable({
	providedIn: 'root'
})
export class TaskGuard implements CanDeactivate<TaskListComponent> {
	canDeactivate(component: TaskListComponent): boolean {
        // if some task is being edited, the <app-task-update> element is in DOM
        const taskUpdates: HTMLCollectionOf<Element> = document.getElementsByTagName('app-task-update');
        if(component.isTaskCreation || taskUpdates.length != 0)
        {
            return window.confirm('Changes you made may not be saved. Leave site?');
        }
		return true;
	}
}

