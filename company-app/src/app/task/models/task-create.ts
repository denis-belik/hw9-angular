export interface TaskCreate {
    name: string;
    description: string;
    createdAt: Date;
    finishedAt: Date;
    projectId: number;
    performerId: number;
}