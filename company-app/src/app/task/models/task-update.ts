import { TaskState } from './task-state'

export interface TaskUpdate {
    id: number;
    name: string;
    description: string;
    finishedAt?: Date;
    state?: TaskState;
    performerId?: number;
}