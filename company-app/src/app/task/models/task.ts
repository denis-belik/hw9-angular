import { TaskState } from './task-state'

export interface Task {
    id: number;
    name: string;
    description: string;
    createdAt: Date;
    finishedAt: Date;
    state: TaskState;
    projectId: number;
    performerId: number;
}