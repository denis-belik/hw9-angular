import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { TaskState } from '../models/task-state';

@Directive({
	selector: '[appTaskStateColor]'
})
export class TaskStateColorDirective implements OnInit {

    @Input('appTaskStateColor') taskState: TaskState;

	constructor(private el: ElementRef<HTMLDivElement>) { }

    ngOnInit(): void {
        let color: string;
        let icon: string;
        switch(this.taskState) {
            case TaskState.Created: 
                color = 'black';
                icon = 'create'
                break;

            case TaskState.Started: 
                color = 'blue';
                icon = 'flag';
                break;
            
            case TaskState.Canceled: 
                color = 'red';
                icon = 'cancel'
                break;
            
            case TaskState.Finished:
                color = 'green';
                icon = 'check';
                break;
        }
        
        this.el.nativeElement.style.color = color;
        this.el.nativeElement.firstElementChild.innerHTML = icon;
    }

}
