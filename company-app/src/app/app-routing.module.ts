import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectListComponent } from './project/components/project-list/project-list.component'
import { UserListComponent } from './user/components/user-list/user-list.component';
import { TaskListComponent } from './task/components/task-list/task-list.component';
import { TeamListComponent } from './team/components/team-list/team-list.component';
import { ProjectGuard } from './project/guards/project.guard'
import { TaskGuard } from './task/guards/task.guard'
import { TeamGuard } from './team/guards/team.guard'
import { UserGuard } from './user/guards/user.guard'

const routes: Routes = [
    { path: '', component: ProjectListComponent, pathMatch: 'full', canDeactivate: [ProjectGuard] },
    { path: 'projects', component: ProjectListComponent, pathMatch: 'full', canDeactivate: [ProjectGuard] },
    { path: 'tasks', component: TaskListComponent, pathMatch: 'full', canDeactivate: [TaskGuard]  },
    { path: 'teams', component: TeamListComponent, pathMatch: 'full', canDeactivate: [TeamGuard] },
    { path: 'users', component: UserListComponent, pathMatch: 'full', canDeactivate: [UserGuard] },
    { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [ProjectGuard]
})
export class AppRoutingModule { }
