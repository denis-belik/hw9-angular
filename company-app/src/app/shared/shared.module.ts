import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomDatePipe } from './pipes/custom-date.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// angular material
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatNativeDateModule } from '@angular/material/core';
import { TaskStatePipe } from './pipes/task-state.pipe';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
	declarations: [CustomDatePipe, TaskStatePipe],
	imports: [
		CommonModule
	],
	exports: [
		CommonModule,
		RouterModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,

        // pipes
		CustomDatePipe,
        TaskStatePipe,
        
		// angular material
		MatButtonModule,
	    MatInputModule,
	    MatDatepickerModule,
	    MatFormFieldModule,
	    MatSelectModule,
	    MatInputModule,
	    MatCardModule,
        MatNativeDateModule,
        MatIconModule,
        MatToolbarModule,
        MatProgressSpinnerModule
	]
})
export class SharedModule { }
