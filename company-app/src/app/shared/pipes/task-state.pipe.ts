import { Pipe, PipeTransform } from '@angular/core';
import { TaskState } from 'src/app/task/models/task-state';

@Pipe({
	name: 'taskState'
})
export class TaskStatePipe implements PipeTransform {

	transform(value: number, ...args: unknown[]): string {
		return TaskState[value];
	}

}
