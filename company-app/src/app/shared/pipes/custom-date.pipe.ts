import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customDate'
})
export class CustomDatePipe implements PipeTransform {

  transform(value: string, ...args: any[]): string {
    const date = new Date(value);
    return `${date.getDate()} ${monthes[date.getMonth()]} ${date.getFullYear()}`;
  }
}

const monthes: string[] = [
    'січня',
    'лютого',
    'березня',
    'квітня',
    'травня',
    'червня',
    'липня',
    'серпня',
    'вересня',
    'жовтня',
    'листопада',
    'грудня'
]