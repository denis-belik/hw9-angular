export interface TeamUpdate {
    id: number;
    name: string;
}