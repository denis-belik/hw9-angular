export interface TeamCreate {
    name: string;
    createdAt: Date;
}