import { NgModule } from '@angular/core';
import { TeamListComponent } from './components/team-list/team-list.component';
import { SharedModule } from '../shared/shared.module';
import { TeamComponent } from './components/team/team.component';
import { TeamCreateComponent } from './components/team-create/team-create.component';
import { TeamUpdateComponent } from './components/team-update/team-update.component';


@NgModule({
	declarations: [TeamListComponent, TeamComponent, TeamCreateComponent, TeamUpdateComponent],
	imports: [
		SharedModule
	]
})
export class TeamModule { }
