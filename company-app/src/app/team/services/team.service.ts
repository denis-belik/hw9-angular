import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { Team } from '../models/team'
import { Observable } from 'rxjs';
import { TeamCreate } from '../models/team-create';
import { HttpResponse } from '@angular/common/http';
import { TeamUpdate } from '../models/team-update';

@Injectable({
	providedIn: 'root'
})
export class TeamService {
    public routePrefix = 'api/teams';

    constructor(private httpService: HttpService) { }
    
    public getTeams(): Observable<Team[]> {
        return this.httpService.get<Team[]>(this.routePrefix);
    }

    public getTeamById(id: number): Observable<Team> {
        return this.httpService.get<Team>(`${this.routePrefix}/${id}`);
    }

    public postTeam(teamCreate: TeamCreate): Observable<HttpResponse<TeamCreate>> {
        return this.httpService.postWithFullResponse<TeamCreate>(this.routePrefix, teamCreate);
    }

    public deleteTeam(team: Team): Observable<Object> {
        return this.httpService.delete(`${this.routePrefix}/${team.id}`);
    }

    public updateTeam(teamUpdate: TeamUpdate): Observable<Object> {
        return this.httpService.put<TeamUpdate>(this.routePrefix, teamUpdate);
    }
}
