import { Component, OnInit, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Team } from '../../models/team';
import { TeamUpdate } from '../../models/team-update';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TeamService } from '../../services/team.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-team-update',
	templateUrl: './team-update.component.html',
	styleUrls: ['./team-update.component.css']
})
export class TeamUpdateComponent implements OnInit, OnDestroy {

    @Input() team: Team;
    @Output() teamUpdated = new EventEmitter<Team>();
    @Output() updateCanceled = new EventEmitter<void>();

    public isLoading: boolean = false;

    public maxNameLength = 32;

    private unsubscribe$ = new Subject<void>();

    public form: FormGroup;
	constructor(
        private teamService: TeamService,
        private toastr: ToastrService) 
    { }

	ngOnInit(): void {
        this.form = new FormGroup({
            name: new FormControl(this.team.name, [Validators.required, Validators.maxLength(this.maxNameLength)]),
        });
	}

    public cancelUpdate(): void {
        this.updateCanceled.emit();
    }

    public onSubmit(): void {
        this.isLoading = true;

        const controls = this.form.controls;
        
        const teamUpdate: TeamUpdate = {
            id: this.team.id,
            name: controls.name.value ? controls.name.value : this.team.name
        }

        this.teamService.updateTeam(teamUpdate)
            .pipe(takeUntil(this.unsubscribe$))    
            .subscribe(() => {
                this.team = {
                    id: this.team.id,
                    createdAt: this.team.createdAt,
                    name: teamUpdate.name,
                }
                
                this.isLoading = false;
                this.toastr.success('Team updated!');
                this.teamUpdated.emit(this.team);
            },
            (error) => {
                this.isLoading = false;
                console.log(error);   
                this.toastr.error('Something went wrong', 'Oops');
            }
        );
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
