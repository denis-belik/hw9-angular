import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Team } from '../../models/team'
import { TeamCreate } from '../../models/team-create'
import { TeamService } from '../../services/team.service';
import { HttpResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-team-create',
	templateUrl: './team-create.component.html',
	styleUrls: ['./team-create.component.css']
})
export class TeamCreateComponent implements OnInit, OnDestroy {
    @Output() teamCreated = new EventEmitter<TeamCreate>();

    public isLoading: boolean = false;

    public maxNameLength = 32;

    private unsubscribe$ = new Subject<void>();

    form = new FormGroup({
        name: new FormControl('', [Validators.required, Validators.maxLength(this.maxNameLength)]),
        createdAt: new FormControl('', [Validators.required])
    });

	constructor(
        private teamService: TeamService,
        private toastr: ToastrService) { }

	ngOnInit(): void {
    }
    
    public onSubmit(): void {
        this.isLoading = true;

        const teamCreate: TeamCreate = <TeamCreate>this.form.value;

        let createdTeam: Team;
        this.teamService.postTeam(teamCreate)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response: HttpResponse<TeamCreate>) => {
                const body: TeamCreate = response.body;
                const teamId: number = this.getIdFromLocation(response.headers.get('location'));

                createdTeam = {
                    id: teamId,
                    name: body.name,
                    createdAt: body.createdAt,
                };
                
                this.isLoading = false;
                this.toastr.success(`Team created!`);
                this.teamCreated.emit(createdTeam);
            },
            (error) => {
                this.isLoading = false;
                console.log(error);   
                this.toastr.error('Something went wrong', 'Oops');
            }        
        );
    }

    private getIdFromLocation(str: string): number {
        const startingIndex: number = str.lastIndexOf('/') + 1;
        return Number(str.slice(startingIndex, str.length));
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
