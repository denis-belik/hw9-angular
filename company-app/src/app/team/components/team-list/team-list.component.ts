import { Component, OnInit, OnDestroy } from '@angular/core';
import { Team } from '../../models/team';
import { TeamService } from '../../services/team.service'
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-team-list',
	templateUrl: './team-list.component.html',
	styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit, OnDestroy {

    public teams: Team[];

    public isTeamCreation: boolean = false;
    public isLoading: boolean = false;

    private unsubscribe$ = new Subject<void>();

	constructor(
        private teamService: TeamService,
        private toastr: ToastrService) { }

	ngOnInit(): void {
        this.isLoading = true;

        this.teamService.getTeams().subscribe(
            (body: Team[]) => {
                this.teams = body;
                this.isLoading = false;
            },
            (error) => {
                this.isLoading = false;
                console.log(error);   
                this.toastr.error('Something went wrong', 'Oops');
            }  
        )
    }
    
    public addTeamToList(team: Team): void {
        this.teams.push(team);
        this.isTeamCreation = false
    }

    public deleteTeam(team: Team): void {
        this.isLoading = true;

        this.teamService.deleteTeam(team)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(() => {    
                this.deleteFromList(team);
                this.toastr.success('Team deleted!');
                this.isLoading = false;
            },
            (error) => {
                this.isLoading = false;
                console.log(error);   
                this.toastr.error('Something went wrong', 'Oops');
            }
        );
    }

    public deleteFromList(team: Team): void {
        const teamIndex: number = this.teams.findIndex((value) => value.id == team.id);
        this.teams.splice(teamIndex, 1);
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
