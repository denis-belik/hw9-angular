import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Team } from '../../models/team'

@Component({
	selector: 'app-team',
	templateUrl: './team.component.html',
	styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {

    @Input() team: Team;

    @Output() teamDelete = new EventEmitter<Team>();

    public isTeamUpdating = false;

	constructor() { }

	ngOnInit(): void {
	}

    public onDelete(): void {
        this.teamDelete.emit(this.team);
    }

    public updateList(updatedTeam: Team): void {
        this.team = updatedTeam;
        this.isTeamUpdating= false;
    }
}