import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { TeamListComponent } from '../components/team-list/team-list.component'

@Injectable({
	providedIn: 'root'
})
export class TeamGuard implements CanDeactivate<TeamListComponent> {
	canDeactivate(component: TeamListComponent): boolean {
        // if some team is being edited, the <app-team-update> element is in DOM
        const teamUpdates: HTMLCollectionOf<Element> = document.getElementsByTagName('app-team-update');
        if(component.isTeamCreation || teamUpdates.length != 0)
        {
            return window.confirm('Changes you made may not be saved. Leave site?');
        }
		return true;
	}
}
