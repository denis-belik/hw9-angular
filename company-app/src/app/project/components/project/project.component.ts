import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Project } from '../../models/project';

@Component({
	selector: 'app-project',
	templateUrl: './project.component.html',
	styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

    @Input() project: Project;

    @Output() projectDelete = new EventEmitter<Project>();

    public isProjectUpdating: boolean = false;

	constructor() { }

	ngOnInit(): void {
	}

    public onDelete(): void {
        this.projectDelete.emit(this.project);
    }

    public updateList(updatedProject: Project): void {
        this.project = updatedProject;
        this.isProjectUpdating= false;
    }
}
