import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ProjectService } from '../../services/project.service';
import { Project } from '../../models/project';
import { ProjectCreate } from '../../models/project-create';
import { Validators } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-project-create',
	templateUrl: './project-create.component.html',
	styleUrls: ['./project-create.component.css']
})
export class ProjectCreateComponent implements OnInit, OnDestroy {
    @Output() projectCreated = new EventEmitter<ProjectCreate>();

    public isLoading: boolean = false;

    private unsubscribe$ = new Subject<void>();

    public maxNameLength = 128;
    public maxDescLength = 1000;
    public minId = 1;

    form = new FormGroup({
        name: new FormControl('', [Validators.required, Validators.maxLength(this.maxNameLength)]),
        description: new FormControl('', [Validators.required, Validators.maxLength(this.maxDescLength)]),
        createdAt: new FormControl('', [Validators.required]),
        deadline: new FormControl('', [Validators.required]),
        authorId: new FormControl('', [Validators.required, Validators.min(this.minId)]),
        teamId: new FormControl('', [Validators.required, Validators.min(this.minId)])
    });

	constructor(
        private projectService: ProjectService,
        private toastr: ToastrService) 
    { }

	ngOnInit(): void {
    }
    
    public onSubmit(): void {
        this.isLoading = true;

        const projectCreate: ProjectCreate = <ProjectCreate>this.form.value;

        let createdProject: Project;
        this.projectService.postProject(projectCreate)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response: HttpResponse<ProjectCreate>) => {
                const body: ProjectCreate = response.body;
                const projectId: number = this.getIdFromLocation(response.headers.get('location'));

                createdProject = {
                    id: projectId,
                    name: body.name,
                    description: body.description,
                    createdAt: body.createdAt,
                    deadline: body.deadline,
                    authorId: body.authorId,
                    teamId: body.teamId
                };
                this.isLoading = false;
                this.toastr.success('Project created!');
                this.projectCreated.emit(createdProject);
                },
                (error) => {
                    this.isLoading = false;
                    console.log(error);   
                    this.toastr.error('Something went wrong', 'Oops');
                }
            );
    }

    private getIdFromLocation(str: string): number {
        const startingIndex: number = str.lastIndexOf('/') + 1;
        return Number(str.slice(startingIndex, str.length));
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
