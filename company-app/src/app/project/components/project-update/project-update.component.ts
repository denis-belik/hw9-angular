import { Component, OnInit, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Project } from '../../models/project';
import { ProjectUpdate } from '../../models/project-update';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProjectService } from '../../services/project.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-project-update',
	templateUrl: './project-update.component.html',
	styleUrls: ['./project-update.component.css']
})
export class ProjectUpdateComponent implements OnInit, OnDestroy {

    @Input() project: Project;
    @Output() projectUpdated = new EventEmitter<Project>();
    @Output() updateCanceled = new EventEmitter<void>();

    public isLoading: boolean = false;

    public maxNameLength = 128;
    public maxDescLength = 1000;
    public minId = 1;

    private unsubscribe$ = new Subject<void>();

    public form: FormGroup;
	constructor(
        private projectService: ProjectService,
        private toastr: ToastrService) { }

	ngOnInit(): void {
        this.form = new FormGroup({
            name: new FormControl(this.project.name, [Validators.required, Validators.maxLength(this.maxNameLength)]),
            description: new FormControl(this.project.description, [Validators.required, Validators.maxLength(this.maxDescLength)]),
            deadline: new FormControl(this.project.deadline, [Validators.required]),
            teamId: new FormControl(this.project.teamId, [Validators.min(this.minId)])
        });
	}

    public cancelUpdate() {
        this.updateCanceled.emit();
    }

    public onSubmit() {
        this.isLoading = true;

        const controls = this.form.controls;
        
        const projectUpdate: ProjectUpdate = {
            id: this.project.id,
            name: controls.name.value ? controls.name.value : this.project.name,
            description: controls.description.value ? controls.description.value : this.project.description,
            deadline: controls.deadline.value ? controls.deadline.value : this.project.deadline,
            teamId: controls.teamId.value ? controls.teamId.value : this.project.teamId
        }

        this.projectService.updateProject(projectUpdate)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(() => {
                this.project = {
                    id: this.project.id,
                    authorId: this.project.authorId,
                    createdAt: this.project.createdAt,
                    name: projectUpdate.name,
                    description: projectUpdate.description,
                    deadline: projectUpdate.deadline,
                    teamId: projectUpdate.teamId
                }
                
                this.isLoading = false;
                this.toastr.success('Project updated!');
                this.projectUpdated.emit(this.project);
            },
            (error) => {
                this.isLoading = false;
                console.log(error);   
                this.toastr.error('Something went wrong', 'Oops');
            }
        );
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
