import { Component, OnInit, OnDestroy } from '@angular/core';
import { Project } from '../../models/project';
import { ProjectService } from '../../services/project.service'
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-project-list',
	templateUrl: './project-list.component.html',
	styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit, OnDestroy {

    public projects: Project[];

    public isProjectCreation: boolean = false;
    public isLoading: boolean = false;

    private unsubscribe$ = new Subject<void>();

	constructor(
        private projectService: ProjectService,
        private toastr: ToastrService) { }

	ngOnInit(): void {
        this.isLoading = true;
        this.projectService.getProjects()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((body: Project[]) => {
                this.projects = body;
                this.isLoading = false;
            },
            (error) => {
                this.isLoading = false;
                console.log(error);   
                this.toastr.error('Something went wrong', 'Oops');
            }     
        );
    }
    
    public addProjectToList(project: Project): void {
        this.projects.push(project);
        this.isProjectCreation = false
    }

    public deleteProject(project: Project): void {
        this.isLoading = true;

        this.projectService.deleteProject(project)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                () => {
                    this.deleteFromList(project);
                    this.toastr.success('Project deleted!');
                    this.isLoading = false;
                },
                (error) => {
                    this.isLoading = false;
                    console.log(error);   
                    this.toastr.error('Something went wrong', 'Oops');
                }
        );
    }

    public deleteFromList(project: Project): void {
        const projectIndex: number = this.projects.findIndex((value) => value.id == project.id);
        this.projects.splice(projectIndex, 1);
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
