export interface ProjectCreate {
    name: string;
    description: string;
    createdAt: Date;
    deadline: Date;
    authorId: number;
    teamId: number;
}