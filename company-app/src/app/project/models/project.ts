export interface Project {
    id: number;
    name: string;
    description: string;
    createdAt: Date;
    deadline: Date;
    authorId: number;
    teamId: number;

    // author: User;
    // team: Team;
    // tasks: Task[];
}