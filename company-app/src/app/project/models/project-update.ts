export interface ProjectUpdate {
    id: number;
    name: string;
    description: string;
    deadline: Date;
    teamId?: number;
}