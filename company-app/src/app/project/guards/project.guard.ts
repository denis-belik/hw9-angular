import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { ProjectListComponent } from '../components/project-list/project-list.component'

@Injectable({
	providedIn: 'root'
})
export class ProjectGuard implements CanDeactivate<ProjectListComponent> {
	canDeactivate(component: ProjectListComponent): boolean {
        // if some project is being edited, the <app-project-update> element is in DOM
        const projectUpdates: HTMLCollectionOf<Element> = document.getElementsByTagName('app-project-update');
        if(component.isProjectCreation || projectUpdates.length != 0)
        {
            return window.confirm('Changes you made may not be saved. Leave site?');
        }
		return true;
	}
}
