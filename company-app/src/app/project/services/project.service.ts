import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { Project } from '../models/project'
import { Observable } from 'rxjs';
import { ProjectCreate } from '../models/project-create';
import { HttpResponse } from '@angular/common/http';
import { ProjectUpdate } from '../models/project-update';

@Injectable({
	providedIn: 'root'
})
export class ProjectService {
    public routePrefix = 'api/projects';

    constructor(private httpService: HttpService) { }
    
    public getProjects(): Observable<Project[]> {
        return this.httpService.get<Project[]>(this.routePrefix);
    }

    public getProjectById(id: number): Observable<Project> {
        return this.httpService.get<Project>(`${this.routePrefix}/${id}`);
    }

    public postProject(projectCreate: ProjectCreate): Observable<HttpResponse<ProjectCreate>> {
        return this.httpService.postWithFullResponse<ProjectCreate>(this.routePrefix, projectCreate);
    }

    public deleteProject(project: Project) {
        return this.httpService.delete(`${this.routePrefix}/${project.id}`);
    }

    public updateProject(projectUpdate: ProjectUpdate) {
        return this.httpService.put<ProjectUpdate>(this.routePrefix, projectUpdate);
    }
}
