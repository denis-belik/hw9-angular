import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectListComponent } from './components/project-list/project-list.component';
import { ProjectComponent } from './components/project/project.component';
import { RouterModule } from '@angular/router';
import { ProjectCreateComponent } from './components/project-create/project-create.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProjectUpdateComponent } from './components/project-update/project-update.component';
import { SharedModule } from '../shared/shared.module'

@NgModule({
	declarations: [ProjectListComponent, ProjectComponent, ProjectCreateComponent, ProjectUpdateComponent],
	imports: [
        SharedModule
	]
})
export class ProjectModule { }
