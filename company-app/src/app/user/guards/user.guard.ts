import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { UserListComponent } from '../components/user-list/user-list.component'

@Injectable({
	providedIn: 'root'
})
export class UserGuard implements CanDeactivate<UserListComponent> {
	canDeactivate(component: UserListComponent): boolean {
        // if some user is being edited, the <app-user-update> element is in DOM
        const userUpdates: HTMLCollectionOf<Element> = document.getElementsByTagName('app-user-update');
        if(component.isUserCreation || userUpdates.length != 0)
        {
            return window.confirm('Changes you made may not be saved. Leave site?');
        }
		return true;
	}
}
