import { NgModule } from '@angular/core';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserComponent } from './components/user/user.component';
import { UserCreateComponent } from './components/user-create/user-create.component';
import { UserUpdateComponent } from './components/user-update/user-update.component';
import { SharedModule } from '../shared/shared.module';



@NgModule({
	declarations: [UserListComponent, UserComponent, UserCreateComponent, UserUpdateComponent],
	imports: [
		SharedModule
	]
})
export class UserModule { }
