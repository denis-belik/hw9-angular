import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { User } from '../models/user'
import { Observable } from 'rxjs';
import { UserCreate } from '../models/user-create';
import { HttpResponse } from '@angular/common/http';
import { UserUpdate } from '../models/user-update';

@Injectable({
	providedIn: 'root'
})
export class UserService {
    public routePrefix = 'api/users';

    constructor(private httpService: HttpService) { }
    
    public getUsers(): Observable<User[]> {
        return this.httpService.get<User[]>(this.routePrefix);
    }

    public getUserById(id: number): Observable<User> {
        return this.httpService.get<User>(`${this.routePrefix}/${id}`);
    }

    public postUser(userCreate: UserCreate): Observable<HttpResponse<UserCreate>> {
        return this.httpService.postWithFullResponse<UserCreate>(this.routePrefix, userCreate);
    }

    public deleteUser(user: User): Observable<Object> {
        return this.httpService.delete(`${this.routePrefix}/${user.id}`);
    }

    public updateUser(userUpdate: UserUpdate): Observable<Object>  {
        return this.httpService.put<UserUpdate>(this.routePrefix, userUpdate);
    }
}
