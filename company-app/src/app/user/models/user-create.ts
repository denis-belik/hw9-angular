export interface UserCreate {
    firstName: string;
    lastName: string;
    email: string;
    city: string;
    birthday: Date;
    registeredAt: Date;
}