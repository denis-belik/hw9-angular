export interface UserUpdate {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    city: string;
    teamId?: number;
}