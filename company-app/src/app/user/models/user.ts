export interface User {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    city: string;
    birthday: Date;
    registeredAt: Date;
    teamId?: number;
}