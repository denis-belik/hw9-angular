import { Component, OnInit, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { User } from '../../models/user';
import { UserUpdate } from '../../models/user-update';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-user-update',
	templateUrl: './user-update.component.html',
	styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit, OnDestroy {

    @Input() user: User;
    @Output() userUpdated = new EventEmitter<User>();
    @Output() updateCanceled = new EventEmitter<void>();

    public isLoading: boolean = false;

    public maxNameLength = 32;
    public maxEmailLength = 64;
    public minId = 1;

    private unsubscribe$ = new Subject<void>();

    public form: FormGroup;
	constructor(
        private userService: UserService,
        private toastr: ToastrService) { }

	ngOnInit(): void {
        this.form = new FormGroup({
            firstName: new FormControl(this.user.firstName, [Validators.required, Validators.maxLength(this.maxNameLength), Validators.pattern('[A-Za-z]+')]),
            lastName: new FormControl(this.user.lastName, [Validators.required, Validators.maxLength(this.maxNameLength), Validators.pattern('[A-Za-z]+')]),
            email: new FormControl(this.user.email, [Validators.required, Validators.maxLength(this.maxEmailLength), Validators.email]),
            city: new FormControl(this.user.city, [Validators.required, Validators.maxLength(this.maxNameLength)]),
            teamId: new FormControl(this.user.teamId, [Validators.min(this.minId)]),
        });
	}

    public cancelUpdate(): void {
        this.updateCanceled.emit();
    }

    public onSubmit(): void {
        this.isLoading = true;

        const controls: { [key:string]: AbstractControl  } = this.form.controls;
        
        const userUpdate: UserUpdate = {
            id: this.user.id,
            firstName: controls.firstName.value ? controls.firstName.value : this.user.firstName,
            lastName: controls.lastName.value ? controls.lastName.value : this.user.lastName,
            email: controls.email.value ? controls.email.value : this.user.email,
            city: controls.city.value ? controls.city.value : this.user.city,
            teamId: controls.teamId.value ? controls.teamId.value : this.user.teamId
        }

        this.userService.updateUser(userUpdate)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(() => { 
                this.user = {
                    id: this.user.id,
                    birthday: this.user.birthday,
                    registeredAt: this.user.registeredAt,
                    firstName: userUpdate.firstName,
                    lastName: userUpdate.lastName,
                    email: userUpdate.email,
                    city: userUpdate.city,
                    teamId: userUpdate.teamId
                }
                
                this.isLoading = false;
                this.toastr.success('User updated!');
                this.userUpdated.emit(this.user);
            },
            (error) => {
                this.isLoading = false;
                console.log(error);   
                this.toastr.error('Something went wrong', 'Oops');
            }
        );
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
