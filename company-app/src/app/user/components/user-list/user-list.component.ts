import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service'
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-user-list',
	templateUrl: './user-list.component.html',
	styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, OnDestroy {

    public users: User[];

    public isUserCreation: boolean = false;
    public isLoading: boolean = false;

    private unsubscribe$ = new Subject<void>();

	constructor(
        private userService: UserService,
        private toastr: ToastrService) { }

	ngOnInit(): void {
        this.isLoading = true;

        this.userService.getUsers().subscribe(
            (body: User[]) => {
                this.users = body;
                this.isLoading = false;
            },
            (error) => {
                this.isLoading = false;
                console.log(error);   
                this.toastr.error('Something went wrong', 'Oops');
            }     
        )
    }
    
    public addUserToList(user: User): void {
        this.users.push(user);
        this.isUserCreation = false;
    }

    public deleteUser(user: User): void {
        this.isLoading = true;

        this.userService.deleteUser(user)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(() => {
                this.deleteFromList(user);
                this.toastr.success('User deleted!');
                this.isLoading = false;
            },
            (error) => {
                this.isLoading = false;
                console.log(error);   
                this.toastr.error('Something went wrong', 'Oops');
            }
        );
    }

    public deleteFromList(user: User): void {
        const userIndex: number = this.users.findIndex((value) => value.id == user.id);
        this.users.splice(userIndex, 1);
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}

