import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { UserCreate } from '../../models/user-create';
import { Validators } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-user-create',
	templateUrl: './user-create.component.html',
	styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit, OnDestroy {
    @Output() userCreated = new EventEmitter<UserCreate>();

    public isLoading: boolean = false;

    public maxNameLength = 32;
    public maxEmailLength = 64;

    private unsubscribe$ = new Subject<void>();

    form = new FormGroup({
        firstName: new FormControl('', [Validators.required, Validators.maxLength(this.maxNameLength), Validators.pattern('[A-Za-z]+')]),
        lastName: new FormControl('', [Validators.required, Validators.maxLength(this.maxNameLength), Validators.pattern('[A-Za-z]+')]),
        email: new FormControl('', [Validators.required, Validators.maxLength(this.maxEmailLength), Validators.email]),
        city: new FormControl('', [Validators.required, Validators.maxLength(this.maxNameLength)]),
        birthday: new FormControl('', [Validators.required]),
        registeredAt: new FormControl('', [Validators.required]),
    });

	constructor(
        private userService: UserService,
        private toastr: ToastrService) { }

	ngOnInit(): void {
    }
    
    public onSubmit(): void {
        this.isLoading = true;

        const userCreate: UserCreate = <UserCreate>this.form.value;

        let createdUser: User;
        this.userService.postUser(userCreate)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response: HttpResponse<UserCreate>) => {
                const body: UserCreate = response.body;
                const userId: number = this.getIdFromLocation(response.headers.get('location'));

                createdUser = {
                    id: userId,
                    firstName: body.firstName,
                    lastName: body.lastName,
                    email: body.email,
                    city: body.city,
                    birthday: body.birthday,
                    registeredAt: body.registeredAt,
                };

                this.isLoading = false;
                this.toastr.success('User created!');
                this.userCreated.emit(createdUser);
            },
            (error) => {
                this.isLoading = false;
                console.log(error);   
                this.toastr.error('Something went wrong', 'Oops');
            }
        )
    }

    private getIdFromLocation(str: string): number {
        const startingIndex: number = str.lastIndexOf('/') + 1;
        return Number(str.slice(startingIndex, str.length));
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
