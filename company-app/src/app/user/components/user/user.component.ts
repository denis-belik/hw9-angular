import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../../models/user';

@Component({
	selector: 'app-user',
	templateUrl: './user.component.html',
	styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

    @Input() user: User;

    @Output() userDelete = new EventEmitter<User>();

    public isUserUpdating: boolean = false;

	constructor() { }

	ngOnInit(): void {
	}

    public onDelete(): void {
        this.userDelete.emit(this.user);
    }

    public updateList(updatedUser: User): void {
        this.user = updatedUser;
        this.isUserUpdating= false;
    }
}
