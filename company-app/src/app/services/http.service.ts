import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class HttpService {

    public baseUrl: string = 'https://localhost:44365/';
    
    constructor(private httpClient: HttpClient) { }
    
    public get<T>(url: string): Observable<T> {
        return this.httpClient.get<T>(this.baseUrl + url);
    }

    public post<T>(url: string, body: T): Observable<T> {
        return this.httpClient.post<T>(this.baseUrl + url, body);
    }

    public postWithFullResponse<T>(url: string, body: T): Observable<HttpResponse<T>> {
        return this.httpClient.post<T>(this.baseUrl + url, body, { observe: 'response' });
    }

    public delete<T>(url: string) {
        return this.httpClient.delete(this.baseUrl + url);
    }

    public put<T>(url: string, body: T) {
        return this.httpClient.put(this.baseUrl + url, body);
    }
}
